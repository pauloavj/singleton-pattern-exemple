import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        ConfigurationSettingsSingleton config = ConfigurationSettingsSingleton.INSTANCE;
        config.setPathToFolder("anything.txt");

        FileProcessor fileProcessor = new FileProcessor();
        fileProcessor.PrintContents(config.getPathToFolder());
    }
}
