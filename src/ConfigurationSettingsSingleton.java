public enum ConfigurationSettingsSingleton {
    INSTANCE;
    private String pathToFolder;

    public String getPathToFolder() {
        return pathToFolder;
    }

    public void setPathToFolder(String pathToFolder) {
        this.pathToFolder = pathToFolder;
    }
}
