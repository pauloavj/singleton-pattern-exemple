import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileProcessor{
    public void PrintContents(String fileName) {
        try(
                FileReader input = new FileReader(fileName);
                BufferedReader reader = new BufferedReader(input);
        ){
            String line;
            while ((line= reader.readLine())!=  null){
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
